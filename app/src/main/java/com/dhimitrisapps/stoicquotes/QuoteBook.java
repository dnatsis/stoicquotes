package com.dhimitrisapps.stoicquotes;
import java.util.Random;

public class QuoteBook {
    // Fields or Member variables - Properties about the object
    private String[] quotes = {
            "If it is not right, do not do it, if it is not true, do not say it.",
            "Think of the life you have lived until now as over and, " +
                    "as a dead man, see what’s left as a bonus and live it according to Nature. " +
                    "Love the hand that fate deals you and play it as your own, for what could be more fitting?",
            "You could leave life right now. Let that determine what you do and say and think.",
            "Be tolerant with others and strict with yourself.",
            "We are more often frightened than hurt; and we suffer more in imagination than in reality.",
            "If a man knows not which port he sails, no wind is favorable.",
            "How does it help…to make troubles heavier by bemoaning them?",
            "How long are you going to wait before you demand the best for yourself?",
            "First say to yourself what you would be; and then do what you have to do.",
            "Don’t explain your philosophy. Embody it." };

    // Methods - Actions the object can take
    String getFact() {
        //randomly select a fact to display
        Random randomGenerator = new Random();
        int randomNumber = randomGenerator.nextInt(quotes.length);
        return quotes[randomNumber];
    }
}
